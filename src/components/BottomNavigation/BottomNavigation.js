import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import Icon from '@material-ui/core/Icon';
import { withRouter } from 'react-router-dom';
import { getSelectedTab } from '../../utils';

const useStyles = makeStyles({
  root: {
    width: '100%',
    aligntItems: 'center',
    justifyContent: 'center',
    position: 'fixed',
    bottom: 0
  }
});

function AppBottomNavigation({ history }) {
  const path = history && history.location && history.location.pathname;
  const classes = useStyles();
  const [value, setValue] = React.useState(getSelectedTab(path));

  return (
    <BottomNavigation
      position="fixed"
      value={value}
      onChange={(event, newValue) => {
        setValue(newValue);
      }}
      showLabels
      className={classes.root}
    >
      <BottomNavigationAction onClick={() => history.push({ pathname: '/' })} label="Books" icon={<Icon>book</Icon>} />
      <BottomNavigationAction
        onClick={() => history.push({ pathname: '/shelves' })}
        label="Shelves"
        icon={<Icon>collections_bookmark</Icon>}
      />
      <BottomNavigationAction
        onClick={() => history.push({ pathname: '/reviews' })}
        label="Reviews"
        icon={<Icon>rate_review</Icon>}
      />
    </BottomNavigation>
  );
}
export default withRouter(AppBottomNavigation);
