import React from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => createStyles({
  root: {
    minWidth: 300,
    maxWidth: 'auto',
    boxShadow: 'none'
  }
}));
function TextWithSubtitle({ title, subtitle, children }) {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      {children}
      <Typography color="textPrimary" variant="body2">
        {title}
      </Typography>
      <Typography variant="body2" color="textSecondary" component="p">
        {subtitle}
      </Typography>
      <br />
    </div>
  );
}

export default TextWithSubtitle;
