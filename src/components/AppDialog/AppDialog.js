import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function AppDialog(props) {
  const { isOpen, title, children, handleClose, onSubmit } = props;

  const handleSubmit = () => {
    handleClose();
    onSubmit();
  };

  return (
    <div>
      <Dialog open={isOpen} onClose={handleClose}>
        <DialogTitle>{title}</DialogTitle>
        <DialogContent>{children}</DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="inherit" autoFocus>
            Close
          </Button>
          {onSubmit && (
            <Button onClick={handleSubmit} color="default" autoFocus>
              Ok
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </div>
  );
}
