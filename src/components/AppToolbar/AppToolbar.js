import React, { useEffect } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import Switch from '@material-ui/core/Switch';
import { FormControlLabel } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import appThemeActions from '../../redux/AppTheme/AppThemeActions';

const useStyles = makeStyles(theme => createStyles({
    root: {
      flexGrow: 1
    },
    menuButton: {
      marginRight: theme.spacing(2)
    },
    title: {
      flexGrow: 1,
      display: 'none',
      [theme.breakpoints.up('sm')]: {
        display: 'block'
      }
    }
  }));

function AppToolbar({ setTheme }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const isDarkMode = useSelector(state => state.themeReducer.isDarkMode);

  const handleChange = event => {
    dispatch(appThemeActions.setDarkMode(event.target.checked));
  };

  useEffect(() => {
    setTheme(isDarkMode);
  }, [isDarkMode]);

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="open drawer">
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap>
            Digital Home Library
          </Typography>
          <FormControlLabel
            control={<Switch onChange={handleChange} checked={isDarkMode} value="checkedB" />}
            label="Dark Mode"
          />
        </Toolbar>
      </AppBar>
    </div>
  );
}
export default AppToolbar;
