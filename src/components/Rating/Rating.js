import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';

const useStyles = makeStyles(() => createStyles({
    root: {
      width: 'auto',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center'
    }
  }));

export default function RatingComponent({ getRating, value, isReadOnly }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Rating readOnly={isReadOnly} name="hover-feedback" value={value} precision={1} onChange={getRating} />
    </div>
  );
}
