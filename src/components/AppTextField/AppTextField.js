import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { TextField } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: theme.spacing(3)
  },
  field: { margin: theme.spacing(1), minWidth: '100%' }
}));

export default function AppTextField({ value, handleChange, multiline, rows, label }) {
  const classes = useStyles();

  return (
    <div>
      <form className={classes.container}>
        <TextField
          onChange={handleChange}
          className={classes.field}
          label={label}
          variant="outlined"
          value={value}
          multiline={multiline}
          rows={rows}
        />
      </form>
    </div>
  );
}
