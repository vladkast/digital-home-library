import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import ButtonBase from '@material-ui/core/ButtonBase';
import Avatar from '@material-ui/core/Avatar';
import { green } from '@material-ui/core/colors';
import { Button } from '@material-ui/core';

const useStyles = makeStyles(theme => createStyles({
    paper: {
      padding: theme.spacing(2),
      minHeight: 120
    },
    button: { margin: 10, minWidth: 150 },
    square: {
      color: theme.palette.getContrastText(green[500]),
      backgroundColor: green[500],
      width: theme.spacing(15),
      height: theme.spacing(15)
    },
    image: {
      width: 128,
      height: 128
    },
    img: {
      margin: 'auto',
      display: 'block',
      maxWidth: '100%',
      maxHeight: '100%'
    }
  }));

function Book({ bookData, showDetails, inShelf, deleteBook, openShelvesModal }) {
  const classes = useStyles();
  const { categories, title, authors, thumbnailUrl } = bookData;
  return (
    <Grid item xs={4}>
      <Paper className={classes.paper}>
        <Grid container spacing={2}>
          <ButtonBase className={classes.image}>
            {thumbnailUrl ? (
              <img className={classes.img} alt="" src={thumbnailUrl} />
            ) : (
              <Avatar variant="square" className={classes.square}>
                {title && title[0]}
              </Avatar>
            )}
          </ButtonBase>
          <Grid item xs container direction="column" spacing={2}>
            <Grid item>
              <Typography variant="subtitle1">{title}</Typography>
              {authors && authors.length && (
                <Typography variant="body2" color="textSecondary">
                  Author: {authors[0]}
                </Typography>
              )}
              {categories && categories.length > 0 && (
                <Typography variant="body2" gutterBottom>
                  Category • {categories}
                </Typography>
              )}
            </Grid>
            <Grid justify="center" container direction="row" spacing={2}>
              <Button
                onClick={() => showDetails(bookData)}
                color="primary"
                className={classes.button}
                variant="contained"
              >
                More details
              </Button>
              {inShelf ? (
                <Button
                  onClick={() => deleteBook(bookData.title)}
                  color="secondary"
                  className={classes.button}
                  variant="contained"
                >
                  Delete Book
                </Button>
              ) : (
                <Button
                  onClick={() => openShelvesModal(bookData)}
                  color="secondary"
                  className={classes.button}
                  variant="contained"
                >
                  Add to Shelf
                </Button>
              )}
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    </Grid>
  );
}

export default Book;
