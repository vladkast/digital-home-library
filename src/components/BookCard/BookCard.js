import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextWithSubtitle from '../TextWithSubtitle/TextWithSubtitle';
import ReviewsList from '../ReviewsList/ReviewsList';

const useStyles = makeStyles(theme => createStyles({
    root: {
      maxWidth: 700,
      minWidth: 400,
      boxShadow: 'none'
    },
    media: {
      height: 0,
      paddingTop: '56.25%' // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest
      })
    },
    expandOpen: {
      transform: 'rotate(180deg)'
    }
  }));

export default function BookCard({ data, reviews }) {
  const classes = useStyles();
  const [expanded, setExpanded] = React.useState(false);
  const { title, shortDescription, longDescription, publishedDate, isbn, authors, pageCount, categories } = data;
  const dateTime = (publishedDate && publishedDate.$date && new Date(publishedDate.$date)) || '';

  const handleExpandClick = () => {
    setExpanded(!expanded);
  };

  return (
    <Card className={classes.root}>
      <CardHeader title={title} subheader={`Released in  ${dateTime && dateTime.getFullYear()}`} />
      <CardContent>
        <TextWithSubtitle title="ISBN" subtitle={isbn} />
        <Typography variant="h6">Authors</Typography>
        <Typography variant="subtitle2" color="textSecondary" component="p" />
        {authors &&
          authors.length &&
          authors.map(author => (
            <Typography key={author} variant="subtitle2" color="textSecondary" component="p">
              {author}
            </Typography>
          ))}
        <br />
        {categories && categories.length && <TextWithSubtitle title="Category" subtitle={categories[0]} />}
        {pageCount > 0 && <TextWithSubtitle title="Page Count" subtitle={pageCount} />}
        {shortDescription && <TextWithSubtitle title="Short Description" subtitle={shortDescription} />}
        {reviews && reviews.length > 0 ? (
          <>
            <Typography style={{ padding: 5 }} variant="overline" color="textSecondary" component="p">
              Shelf Reviews
            </Typography>
            <ReviewsList reviews={reviews} />
          </>
        ) : null}
      </CardContent>
      {longDescription && (
        <>
          <CardActions disableSpacing>
            <IconButton
              className={clsx(classes.expand, {
                [classes.expandOpen]: expanded
              })}
              onClick={handleExpandClick}
              aria-expanded={expanded}
              aria-label="show more"
            >
              <ExpandMoreIcon />
            </IconButton>
            Show more ...
          </CardActions>
          <Collapse in={expanded} timeout="auto" unmountOnExit>
            <CardContent>
              <TextWithSubtitle title="Long Description" subtitle={longDescription} />
            </CardContent>
          </Collapse>
        </>
      )}
    </Card>
  );
}
