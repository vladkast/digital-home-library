import React from 'react';
import { Grid } from '@material-ui/core';
import TextWithSubtitle from '../TextWithSubtitle/TextWithSubtitle';
import RatingComponent from '../Rating/Rating';

const ReviewsList = ({ reviews }) => reviews.map(({ comment, date, stars }) => {
    const dateTime = new Date(date);
    const dateString = dateTime && dateTime.toLocaleDateString();
    const time = dateTime && dateTime.toLocaleTimeString();
    return (
      reviews &&
      reviews.length && (
        <Grid key={date} item xs={6} direction="row">
          <TextWithSubtitle title={comment} subtitle={`${dateString} ${time}`}>
            <RatingComponent isReadOnly key={date} value={stars} />
          </TextWithSubtitle>
        </Grid>
      )
    );
  });

export default ReviewsList;
