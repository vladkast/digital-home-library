import React, { useEffect } from 'react';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(theme => createStyles({
    formControl: {
      margin: theme.spacing(1),
      minWidth: 500
    },
    selectEmpty: {
      marginTop: theme.spacing(2)
    }
  }));

export default function Dropdown({ options, label, value, handleChange }) {
  const classes = useStyles();

  const inputLabel = React.useRef(null);
  const [labelWidth, setLabelWidth] = React.useState(0);
  useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, []);

  return (
    <FormControl variant="outlined" className={classes.formControl}>
      <InputLabel ref={inputLabel}>{label}</InputLabel>
      <Select value={value} onChange={handleChange} labelWidth={labelWidth}>
        {options &&
          options.map(option => (
            <MenuItem key={option.title} value={option}>
              {option.title}
              <Typography style={{ padding: 5 }} variant="body2" color="textSecondary" component="p">
                {` ${option.categories[0]}`}
              </Typography>
            </MenuItem>
          ))}
        <MenuItem value="">
          <em>None</em>
        </MenuItem>
      </Select>
    </FormControl>
  );
}
