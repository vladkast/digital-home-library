export const updateState = (oldObject, updatedProperties) => ({ ...oldObject, ...updatedProperties });

export const getShelfTitle = (index, length) => {
  if (length > 0 && length !== 1) {
    return `Shelf ${index} has ${length} Books`;
  }
  if (length === 1) {
    return `Shelf ${index} has ${length} Book`;
  }
  if (length === 0) {
    return `Shelf ${index} is empty`;
  }
  return '';
};

export const getSelectedTab = path => {
  if (path === '/') {
    return 0;
  }
  if (path === '/shelves') {
    return 1;
  }
  if (path === '/reviews') {
    return 2;
  }
  return 0;
};
