const books = require('./books.json');
const express = require('express');

const server = express();
const port = 3001;

server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

server.get('/books', (req, res) => res.json(books));

// eslint-disable-next-line no-console
server.listen(port, () => console.log(`App service is listening on port ${port}!`));
