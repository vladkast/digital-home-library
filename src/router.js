import React, { Suspense } from 'react';
import { Route, withRouter, Switch } from 'react-router-dom';

const BookList = React.lazy(() => import('./containers/BookList/BookList'));
const Shelves = React.lazy(() => import('./containers/Shelves/Shelves'));

const Routes = () => (
  <Suspense fallback={'Loading...'}>
    <Switch>
      <Route exact path={'/'} component={BookList} />
      <Route exact path={'/shelves'} component={Shelves} />
    </Switch>
  </Suspense>
);
export default withRouter(Routes);
