const appThemeActions = {
  SET_DARK_MODE: 'SET_DARK_MODE',

  setDarkMode: theme => dispatch => dispatch({ type: appThemeActions.SET_DARK_MODE, payload: theme })
};

export default appThemeActions;
