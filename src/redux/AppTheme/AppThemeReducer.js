import { updateState } from '../../utils';
import AppThemeActions from './AppThemeActions';

const initialState = {
  isDarkMode: true
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AppThemeActions.SET_DARK_MODE:
      return updateState(state, { isDarkMode: action.payload });

    default:
      return state;
  }
};
