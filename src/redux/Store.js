import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import rootReducer from './Reducer';

const persistConfig = {
  key: 'root',
  storage,
  // blacklist: ['shelvesReducer', 'booksReducer']
};
const reducer = persistReducer(persistConfig, rootReducer);
const middleware = applyMiddleware(thunk);
const store = createStore(reducer, middleware);
const persistor = persistStore(store);
export { persistor, store };
