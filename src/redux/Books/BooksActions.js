const booksActions = {
  SET_BOOKS: 'SET_BOOKS',
  DELETE_BOOK: 'DELETE_BOOK',

  setBooks: () => dispatch => {
    fetch('http://localhost:3001/books')
      .then(data => data.json())
      .then(books => {
        dispatch({ type: booksActions.SET_BOOKS, payload: books });
      })
      .catch(err => err);
  },
  addBook: book => dispatch => dispatch({ type: booksActions.ADD_BOOK, payload: book }),
  deleteBook: book => dispatch => dispatch({ type: booksActions.DELETE_BOOK, payload: book })
};

export default booksActions;
