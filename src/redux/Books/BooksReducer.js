import { updateState } from '../../utils';
import bookActions from './BooksActions';

const initialState = {
  books: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case bookActions.SET_BOOKS:
      return updateState(state, { books: action.payload });

    default:
      return state;
  }
};
