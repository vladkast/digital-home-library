import { combineReducers } from 'redux';
import ShelvesReducer from './Shelves/ShelvesReducer';
import BookReducer from './Books/BooksReducer';
import AppThemeReducer from './AppTheme/AppThemeReducer';

const allReducers = combineReducers({
  shelvesReducer: ShelvesReducer,
  booksReducer: BookReducer,
  themeReducer: AppThemeReducer
});

const appReducer = (state, action) => allReducers(state, action);
export default appReducer;
