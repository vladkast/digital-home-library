import { updateState } from '../../utils';
import shelvesActions from './ShelvesActions';

const initialState = {
  shelvesError: null,
  shelvesSuccess: null,
  shelves: [
    { name: 'Web Development', books: [], reviews: [] },
    { name: 'Java', books: [], reviews: [] },
    { name: 'Internet', books: [], reviews: [] },
    { name: 'Python', books: [], reviews: [] }
  ]
};

export default (state = initialState, action) => {
  switch (action.type) {
    case shelvesActions.ADD_SHELF:
      return updateState(state, { shelves: [...state.shelves, action.payload] });

    case shelvesActions.DELETE_SHELF:
      return updateState(state, {
        shelves: state.shelves.filter(category => category.name !== action.payload)
      });
    case shelvesActions.ADD_BOOK:
      return updateState(state, { shelves: action.payload });

    case shelvesActions.ADD_COMMENT:
      return updateState(state, { shelves: action.payload });

    case shelvesActions.DELETE_BOOK:
      return updateState(state, { shelves: action.payload });

    case shelvesActions.SET_ERROR:
      return updateState(state, { shelvesError: action.payload });

    case shelvesActions.SET_SUCCESS:
      return updateState(state, { shelvesSuccess: action.payload });

    case shelvesActions.RESET_ERROR:
      return updateState(state, { shelvesError: action.payload });

    case shelvesActions.RESET_SUCCESS:
      return updateState(state, { shelvesSuccess: action.payload });

    default:
      return state;
  }
};
