const shelvesActions = {
  ADD_SHELF: 'ADD_SHELF',
  DELETE_SHELF: 'DELETE_SHELF',

  ADD_BOOK: 'ADD_BOOK',
  DELETE_BOOK: 'DELETE_BOOK',

  SET_ERROR: 'SET_ERROR',
  RESET_ERROR: 'RESET_ERROR',

  SET_SUCCESS: 'SET_SUCCESS',
  RESET_SUCCESS: 'SET_SUCCESS',

  ADD_COMMENT: 'ADD_COMMENT',

  addBook: (shelf, newBook, shelves) => dispatch => {
    const currentShelf = shelves.find(category => category.name === shelf);

    if (currentShelf && newBook.categories[0] === shelf) {
      if (currentShelf.books.some(book => book.title === newBook.title)) {
        dispatch({ type: shelvesActions.SET_ERROR, payload: 'BOOK_ALREADY_EXISTS_ERROR' });
        return;
      }

      currentShelf.books.push(newBook);
      dispatch({ type: shelvesActions.ADD_BOOK, payload: shelves });
      dispatch({ type: shelvesActions.SET_SUCCESS, payload: 'BOOK_ADDED_SUCCESS' });
    } else dispatch({ type: shelvesActions.SET_ERROR, payload: 'DIFFERENT_CATEGORY_ERROR' });
  },

  deleteBook: (shelf, bookToDelete, shelves) => dispatch => {
    const currentShelf = shelves.find(category => category.name === shelf);
    if (currentShelf) {
      currentShelf.books = currentShelf.books.filter(book => book.title !== bookToDelete);
      dispatch({ type: shelvesActions.ADD_BOOK, payload: shelves });
    }
  },

  addShelf: category => dispatch => dispatch({ type: shelvesActions.ADD_SHELF, payload: category }),
  deleteShelf: bookDetails => dispatch => dispatch({ type: shelvesActions.DELETE_SHELF, payload: bookDetails }),

  resetError: () => dispatch => dispatch({ type: shelvesActions.RESET_ERROR, payload: null }),
  resetSuccess: () => dispatch => dispatch({ type: shelvesActions.RESET_SUCCESS, payload: null }),

  addComment: (shelf, review, shelves) => dispatch => {
    const currentShelf = shelves.find(category => category.name === shelf);
    if (currentShelf) {
      currentShelf.reviews.push(review);
      dispatch({ type: shelvesActions.ADD_COMMENT, payload: shelves });
    }
  }
};

export default shelvesActions;
