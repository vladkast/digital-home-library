import React, { useState } from 'react';
import RateReview from '@material-ui/icons/RateReview';
import Badge from '@material-ui/core/Badge';
import { Typography, Grid } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import shelvesActions from '../../redux/Shelves/ShelvesActions';
import AddReview from './Components/AddReview/AddReview';
import ReviewsList from '../../components/ReviewsList/ReviewsList';

function Review({ shelfData, allShelves }) {
  const { reviews, name } = shelfData;

  const dispatch = useDispatch();

  const [comment, setComment] = useState();
  const [rating, setRating] = useState();
  const [inReview, setInReview] = useState(false);

  const handleEnableReview = () => setInReview(true);
  const handleCloseReview = () => setInReview(false);
  const handleChangeComment = evt => setComment(evt.target.value);

  const handleChangeRating = (event, newValue) => {
    setRating(newValue);
  };

  const handleAddNewReview = () => {
    const review = { comment, stars: rating, date: new Date() };
    dispatch(shelvesActions.addComment(name, review, allShelves));
    setComment('');
    setComment('');
  };

  const isSaveEnabled = comment && rating;
  const reviewsCount = reviews && reviews.length;
  return (
    <Grid alignItems="center" direction="column" container spacing={2}>
      <br />
      {!inReview && (
        <Badge
          style={{ cursor: 'pointer' }}
          horizontal="right"
          onClick={handleEnableReview}
          color="secondary"
          badgeContent={reviewsCount}
        >
          <Typography style={{ padding: 5, cursor: 'pointer' }} variant="body2" color="textSecondary" component="p">
            Click to review this Shelf
          </Typography>
          <RateReview htmlColor="textPrimary" color="action" />
        </Badge>
      )}
      <br />
      {inReview && (
        <>
          {reviews && reviews.length ? (
            <>
              <Typography style={{ padding: 5 }} variant="overline" color="textSecondary" component="p">
                Reviews
              </Typography>

              <ReviewsList reviews={reviews} />
            </>
          ) : null}

          <AddReview
            rating={rating}
            handleChangeRating={handleChangeRating}
            handleChangeComment={handleChangeComment}
            comment={comment}
            handleClose={handleCloseReview}
            handleAddNewReview={handleAddNewReview}
            isSaveEnabled={isSaveEnabled}
          />
        </>
      )}
    </Grid>
  );
}

export default Review;
