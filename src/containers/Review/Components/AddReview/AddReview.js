import React from 'react';
import { Typography, Grid, Button } from '@material-ui/core';
import RatingComponent from '../Rating/Rating';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import AppTextField from '../../../../components/AppTextField/AppTextField';

const useStyles = makeStyles(() => createStyles({
    button: { margin: 5, minWidth: 100 }
  }));

function AddReview({
  rating,
  handleChangeRating,
  handleChangeComment,
  comment,
  handleClose,
  handleAddNewReview,
  isSaveEnabled
}) {
  const classes = useStyles();
  return (
    <>
      <Typography style={{ padding: 5 }} variant="overline" color="textSecondary" component="p">
        Add New Review
      </Typography>
      <RatingComponent rating={rating} getRating={handleChangeRating} />
      <AppTextField multiline handleChange={handleChangeComment} value={comment} label="Review Comment" />
      <Grid justify="center" container direction="row" spacing={2}>
        <Grid item xs={3}>
          <Button onClick={handleClose} className={classes.button} variant="outlined" color="primary">
            Close
          </Button>

          <Button
            onClick={handleAddNewReview}
            disabled={!isSaveEnabled}
            className={classes.button}
            variant="outlined"
            color="secondary"
          >
            Save
          </Button>
        </Grid>
      </Grid>
    </>
  );
}

export default AddReview;
