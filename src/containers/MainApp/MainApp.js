import React, { useState } from 'react';
import { SnackbarProvider } from 'notistack';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from '../../router';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import AppBottomNavigation from '../../components/BottomNavigation/BottomNavigation';
import AppToolbar from '../../components/AppToolbar/AppToolbar';
import { Provider as Redux } from 'react-redux';
import { store } from '../../redux/Store';

function MainApp() {
  const [isDarkMode, setUserPreference] = useState(false);
  const getTheme = theme => {
    setUserPreference(theme);
  };
  const theme = React.useMemo(
    () => createMuiTheme({
        palette: {
          type: isDarkMode ? 'dark' : 'light'
        }
      }),
    [isDarkMode]
  );
  return (
    <Redux store={store}>
      <SnackbarProvider maxSnack={3}>
        <ThemeProvider theme={theme}>
          <Router>
            <AppToolbar setTheme={getTheme} />
            <Routes />
            <AppBottomNavigation />
          </Router>
        </ThemeProvider>
      </SnackbarProvider>
    </Redux>
  );
}
export default MainApp;
