export const messages = {
  DIFFERENT_CATEGORY_ERROR: "Can't add Book with different Category!",
  BOOK_ALREADY_EXISTS_ERROR: 'Book already exists!',

  BOOK_ADDED_SUCCESS: 'Added Successfully!'
};
