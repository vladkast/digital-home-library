import React, { useState, useEffect, useCallback } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Loader from '../../../../components/Loader/Loader';
import Grid from '@material-ui/core/Grid';
import BookCard from '../../../../components/BookCard/BookCard';
import TextWithSubtitle from '../../../../components/TextWithSubtitle/TextWithSubtitle';
import AppDialog from '../../../../components/AppDialog/AppDialog';
import Book from '../../../../components/Book/Book';
import { Divider, Button } from '@material-ui/core';
import { getShelfTitle } from '../../../../utils';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';
import { useSnackbar, withSnackbar } from 'notistack';
import shelvesActions from '../../../../redux/Shelves/ShelvesActions';
import Review from '../../../Review/Review';
import Dropdown from '../../../../components/Dropdown/Dropdown';
import { messages } from './constants';

const useStyles = makeStyles(theme => createStyles({
  root: {
    padding: theme.spacing(2),
    flexGrow: 1,
    height: '100%'
  },
  button: { margin: 5 },
  toolbar: theme.mixins.toolbar
}));

function Shelf({ shelfData, index }) {
  const { books, name } = shelfData;

  const { enqueueSnackbar } = useSnackbar();
  const dispatch = useDispatch();
  const classes = useStyles();

  const allBooks = useSelector(state => state.booksReducer.books);
  const allShelves = useSelector(state => state.shelvesReducer.shelves);
  const shelvesError = useSelector(state => state.shelvesReducer.shelvesError, shallowEqual);
  const shelvesSuccess = useSelector(state => state.shelvesReducer.shelvesSuccess, shallowEqual);

  const [selectedBook, setSelectedBook] = useState('');
  const [isOpenedBookInfoModal, setOpenedBookInfoModal] = useState(false);
  const [isOpenedBookListModal, setOpenedBookListModal] = useState(false);

  const { deleteShelf, addBook, deleteBook, resetSuccess, resetError } = shelvesActions;

  const handleOpenDetails = bookData => {
    setOpenedBookInfoModal(true);
    setSelectedBook(bookData);
  };

  const handleDeleteShelf = () => {
    dispatch(deleteShelf(shelfData.name));
    displayMessage('Successfully Deleted!', 'success');
  };

  const handleOpenBookListModal = () => setOpenedBookListModal(true);
  const handleCloseBookInfoModal = () => {
    setSelectedBook('');
    setOpenedBookInfoModal(false);
  };

  const displayMessage = useCallback(
    (message, variant) => enqueueSnackbar(message, { variant, preventDuplicate: true }),
    [enqueueSnackbar]
  );

  useEffect(() => {
    if (shelvesError) {
      displayMessage(messages[shelvesError], 'error');
    }
    if (shelvesSuccess) {
      displayMessage(messages[shelvesSuccess], 'success');
    }
    resetMessages();
  }, [displayMessage, shelvesError, shelvesSuccess, allShelves]);

  const handleAddBook = () => {
    if (selectedBook) {
      dispatch(addBook(shelfData.name, selectedBook, allShelves));
      handleCloseBookListModal();
    } else {
      displayMessage('Please select a Book', 'error');
    }
  };

  const handleDeleteBook = book => {
    dispatch(deleteBook(shelfData.name, book, allShelves));
    handleCloseBookListModal();
  };

  const handleCloseBookListModal = () => {
    setOpenedBookListModal(false);
    setSelectedBook('');
  };

  const resetMessages = () => {
    dispatch(resetSuccess());
    dispatch(resetError());
  };

  const handleSelectedBook = evt => setSelectedBook(evt.target.value);

  return (
    <>
      <Divider variant="middle" />
      <br />
      <TextWithSubtitle
        className={classes.text}
        title={getShelfTitle(index + 1, books && books.length)}
        subtitle={`Category • ${name}`}
      />

      <Button className={classes.button} onClick={handleOpenBookListModal} variant="outlined" color="primary">
        Add Book
      </Button>

      <Button className={classes.button} onClick={handleDeleteShelf} variant="outlined" color="secondary">
        Delete Shelf
      </Button>
      <Loader isLoading={false} />

      <AppDialog handleClose={handleCloseBookInfoModal} isOpen={isOpenedBookInfoModal} title="Book Details">
        <BookCard data={selectedBook} reviews={shelfData.reviews} />
      </AppDialog>

      <AppDialog
        onSubmit={handleAddBook}
        handleClose={handleCloseBookListModal}
        isOpen={isOpenedBookListModal}
        title="Add book to Shelf"
      >
        <Dropdown handleChange={handleSelectedBook} value={selectedBook} label={'List Book'} options={allBooks} />
      </AppDialog>

      <Review allShelves={allShelves} shelfData={shelfData} />

      {shelfData && books && books.length > 0 && (
        <div className={classes.root}>
          <div className={classes.toolbar} />
          <Grid container direction="row" spacing={3}>
            {books.map(book => (
              <Book
                inShelf
                deleteBook={handleDeleteBook}
                showDetails={handleOpenDetails}
                key={book.name}
                bookData={book}
              />
            ))}
          </Grid>
        </div>
      )}
      <br />
    </>
  );
}

export default withSnackbar(Shelf);
