import React, { useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Shelf from './Components/Shelf/Shelf';
import { withRouter } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Button } from '@material-ui/core';
import shelvesActions from '../../redux/Shelves/ShelvesActions';
import { useSnackbar, withSnackbar } from 'notistack';
import AppDialog from '../../components/AppDialog/AppDialog';
import AppTextField from '../../components/AppTextField/AppTextField';

const useStyles = makeStyles(theme => createStyles({
  root: {
    padding: theme.spacing(3),
    flexGrow: 1,
    backgroundColor: theme.palette.background.default
  },
  toolbar: theme.mixins.toolbar
}));

function Shelves() {
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();
  const dispatch = useDispatch();
  const shelves = useSelector(state => state.shelvesReducer.shelves);
  const [opened, setOpened] = useState(false);
  const [shelf, setSelectedShelf] = useState();
  const displayMessage = (message, variant) => enqueueSnackbar(message, { variant, preventDuplicate: true });

  const handleOpenShelvesModal = () => {
    setOpened(true);
  };

  const addShelf = () => {
    if (shelf) {
      if (shelves.some(category => category.name.toLowerCase() === shelf.toLowerCase())) {
        displayMessage('Shelf with such Category already exists!', 'error');
        setSelectedShelf('');
      } else {
        dispatch(
          shelvesActions.addShelf({
            name: shelf,
            books: [],
            reviews: []
          })
        );
        displayMessage('Shelf was successfully added', 'success');
        setSelectedShelf('');
      }
    } else {
      displayMessage('Please provide Category Name', 'error');
    }
  };

  const handleClose = () => setOpened(false);
  const handleChange = evt => setSelectedShelf(evt.target.value);

  return (
    <>
      <AppDialog
        onSubmit={addShelf}
        handleChange={handleChange}
        handleClose={handleClose}
        isOpen={opened}
        title="Add New Shelf"
      >
        <AppTextField label={'Category'} value={shelf} handleChange={handleChange} />
      </AppDialog>
      <div className={classes.root}>
        <div className={classes.toolbar} />
        <Button style={{ width: 300, marginBottom: 15 }} variant="outlined" onClick={handleOpenShelvesModal}>
          Add New Shelf
        </Button>
        {shelves.map((shelf, index) => (
          <Shelf key={shelf.name} index={index} shelfData={shelf} />
        ))}
        <div className={classes.toolbar} />
      </div>
    </>
  );
}

export default withRouter(withSnackbar(Shelves));
