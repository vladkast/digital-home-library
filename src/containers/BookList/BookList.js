/* eslint-disable no-sequences */
import React, { useState, useEffect, useCallback } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Book from '../../components/Book/Book';
import Loader from '../../components/Loader/Loader';
import AppDialog from '../../components/AppDialog/AppDialog';
import BookCard from '../../components/BookCard/BookCard';
import { useSelector, useDispatch } from 'react-redux';
import bookActions from '../../redux/Books/BooksActions';
import shelvesActions from '../../redux/Shelves/ShelvesActions';
import Dropdown from './Components/Dropdown/Dropdown';
import TextWithSubtitle from '../../components/TextWithSubtitle/TextWithSubtitle';
import { messages } from '../Shelves/Components/Shelf/constants';
import { useSnackbar, withSnackbar } from 'notistack';

const useStyles = makeStyles(theme => createStyles({
    root: {
      padding: theme.spacing(4),
      flexGrow: 1,
      backgroundColor: theme.palette.background.default
    },
    toolbar: theme.mixins.toolbar
  }));

function BookList() {
  const { addBook, resetSuccess, resetError } = shelvesActions;
  const classes = useStyles();
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const books = useSelector(state => state.booksReducer.books);
  const shelves = useSelector(state => state.shelvesReducer.shelves);
  const shelvesError = useSelector(state => state.shelvesReducer.shelvesError);
  const shelvesSuccess = useSelector(state => state.shelvesReducer.shelvesSuccess);
  const [selectedBook, setSelectedBook] = useState({});
  const [selectedShelf, setSelectedShelf] = useState('');
  const [opened, setOpened] = useState(false);
  const [isOpenedShelvesListModal, setOpenedShelvesListModal] = useState(false);
  const [isLoading, setIsLoading] = React.useState(true);
  const [shelfReviews, setShelfReviews] = useState([]);

  const displayMessage = useCallback(
    (message, variant) => enqueueSnackbar(message, { variant, preventDuplicate: true }),
    [enqueueSnackbar]
  );

  const handleOpenDetails = bookData => {
    setOpened(true);
    setSelectedBook(bookData);
    getReviews(bookData);
  };

  const handleClose = () => {
    setOpened(false);
  };

  const handleOpenShelvesListModal = bookData => {
    setSelectedBook(bookData);
    setOpenedShelvesListModal(true);
  };

  const handleChangeShelf = evt => setSelectedShelf(evt.target.value);

  const handleCloseBookListModal = () => {
    setOpenedShelvesListModal(false);
    setSelectedShelf('');
  };

  const getReviews = bookData => {
    const bookCategory = bookData && bookData.categories && bookData.categories[0];
    const currentShelf = shelves.find(shelf => shelf.name === bookCategory);
    if (currentShelf && currentShelf.books.some(book => book.title === bookData.title)) {
      setShelfReviews(currentShelf.reviews);
    }
  };

  const handleAddBook = () => {
    if (selectedShelf) {
      dispatch(addBook(selectedShelf, selectedBook, shelves));
      handleCloseBookListModal();
    } else {
      displayMessage('Please select Category Name', 'error');
    }
  };

  const resetMessages = () => {
    dispatch(resetSuccess());
    dispatch(resetError());
  };

  useEffect(() => {
    if (shelvesError) {
      displayMessage(messages[shelvesError], 'error');
    }
    if (shelvesSuccess) {
      displayMessage(messages[shelvesSuccess], 'success');
    }
    resetMessages();
    // setTimeout was used to simulate 'long' request to the Local Mock Server data :)))
    setTimeout(() => {
      setIsLoading(true);
      if (!books.length) {
        dispatch(bookActions.setBooks());
      }

      setIsLoading(false);
    }, 500);
  }, [books, dispatch, displayMessage, shelvesError, shelvesSuccess, shelves]);

  const category = selectedBook && selectedBook.categories && selectedBook.categories[0];
  return (
    <>
      <Loader isLoading={isLoading} />

      <AppDialog handleClose={handleClose} isOpen={opened} title="Book Details">
        <BookCard reviews={shelfReviews} data={selectedBook} />
      </AppDialog>

      <AppDialog
        onSubmit={handleAddBook}
        handleClose={handleCloseBookListModal}
        isOpen={isOpenedShelvesListModal}
        title="Add book to Shelf category"
      >
        <TextWithSubtitle title={selectedBook.title} subtitle={`Category • ${category}`} />
        <Dropdown
          data={selectedBook}
          handleChange={handleChangeShelf}
          value={selectedShelf}
          label={'Shelves categories'}
          options={shelves}
        />
      </AppDialog>

      {books && (
        <div className={classes.root}>
          <div className={classes.toolbar} />
          <Grid container direction="row" spacing={3}>
            {books.map(book => (
              <Book
                key={book.name}
                openShelvesModal={handleOpenShelvesListModal}
                showDetails={handleOpenDetails}
                bookData={book}
              />
            ))}
          </Grid>
          <div className={classes.toolbar} />
        </div>
      )}
    </>
  );
}

export default withSnackbar(BookList);
