import React from 'react';
import './App.css';
import MainApp from './containers/MainApp/MainApp';

function App() {
  return (
    <div className="App">
      <MainApp />
    </div>
  );
}

export default App;
