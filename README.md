This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

Start App:

### `yarn start`

Start Mock Server:

### `yarn server` or `npm run server`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.